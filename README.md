<img src="https://miro.medium.com/max/1400/1*ZBTCjHPrPRQqNtPnmwvfRQ.jpeg" width="100%" height="350px" style="object-fit: cover;">

![](https://gitlab.com/majksa/api-template/badges/master/coverage.svg)
![](https://gitlab.com/majksa/api-template/badges/master/pipeline.svg)
![](https://img.shields.io/packagist/dm/majksa/api-template)
![](https://img.shields.io/packagist/v/majksa/api-template)
![](https://img.shields.io/packagist/php-v/majksa/api-template)
![](https://img.shields.io/badge/git-Git%20Lab-orange)
![](https://img.shields.io/badge/api-GraphQL-da0093)
![](https://img.shields.io/badge/database-MariaDB-005e86)

-----

## Goal

Main goal is to provide best prepared GraphQL API starter-kit project in Nette with user registration and login.

Focused on:

- latest PHP 8.0
- mariadb
- `nette/*` packages
- GraphQL via `maxa-ondrej/nette-graphql` (and [graphqlite](https://graphqlite.thecodingmachine.io/docs/queries))
- [Doctrine ORM](https://www.doctrine-project.org/projects/orm.html)
  via [`nettrine/*`](https://contributte.org/nettrine/)
- Symfony components via [`contributte/*`](https://contributte.org)
- codestyle checking via **CodeSniffer** and `ninjify/*`
- static analysing via **phpstan**
- unit / integration tests via [**Nette Tester**](https://tester.nette.org/en/testcase) and `ninjify/*`

# Summary

1. [Initial Configuration](#initial-configuration)
2. [Installation](#installation)
3. [Quality Assurance](#quality-assurance)
    1. [Formatting](#formatting)
    2. [Static Analysis](#static-analysis)
    3. [Running tests](#running-tests)
4. [Database](#database)

# Initial Configuration

Since it is all running inside docker containers, no additional configuration is needed! Though you need to install some
tools first:

- [docker](https://docs.docker.com/get-docker/) and docker compose (included in docker from version 3.4)

## Optional:

- **make** - _for easier scripts configuration_
- **php8.0** and **composer** - _if you want to run tests without having to start docker containers_

# Installation

1. Create a local copy of _docker-compose.local.dist.yml_
    - `cp docker-compose.local.dist.yml docker-compose.local.yml`
2. Start the server and run database migrations:
    1. [Using Makefile](#using-makefile)
    2. [Using docker compose](#using-docker-compose)
3. And the backend is up and running on url: http://localhost:8080/graphql

If you wish to change the port, you can do so in _docker-compose.local.yml_: `services > server > ports`

## Using Makefile

If you have installed Make, you can follow the steps bellow for the easiest setup.

2. `make compose-dev` - starts docker network
3. `make prepare-db` - runs database migrations and generates testing fixtures

## Using docker compose

1. `docker compose -f docker-compose.local.yml up -d --force-recreate --build --remove-orphans` - starts docker network
2. `docker exec -e NETTE_DEBUG=1 apitte-skeleton-app-1 bin/console orm:schema-tool:drop --force --full-database` - drop
   the database schema
3. `docker exec -e NETTE_DEBUG=1 apitte-skeleton-app-1 bin/console migrations:migrate --no-interaction` - runs database
   migrations
4. `docker exec -e NETTE_DEBUG=1 apitte-skeleton-app-1 bin/console doctrine:fixtures:load --no-interaction --append` -
   generates testing fixtures

# Quality Assurance

To make sure our code quality, we use 3 different tools:

- **CodeSniffer**: for formatting
- **PHPStan**: for static analysis
- **Nette** Tester: for running tests

You can simply run all of them using `composer run-script qa`.

**To make sure these tools run before every commit, please run: `git config core.hooksPath .hooks`**

## Formatting

We follow some basic conventions, which are set up in `phpcs.xml` file.

- formatting and optimizing the code, run `composer run-script cbf`
- just analysis, run `composer run-script cs`

## Static Analysis

To prevent common and easy to find mistakes, we run a very useful tool PHPStan.

- `composer run-script phpstan`

## Running tests

Tests are a really important part of coding and exposes most errors before deployment.

To create a new TestCase, you can simply duplicate any existing Test inside `/tests/Unit` folder. \
If you wish to add a new integration test, create a new TestCase in `/tests/Integration` folder.

- running all tests: `composer run-script tests`
- running test coverage: `composer run-script coverage`

## Running GraphQL request

Easiest way to run request to the api is using [Altair](https://altair.sirmuel.design/#download).

### Tracy bar

If you want to display the Tracy bar, that's definitely possible!
Just copy the query _- and variables if there are any-_ and add them inside the [GET] request as `query`
and `variables`:

- http://localhost:8080/graphql?query={hello(name:$name)}&variables={"name":%20"Hello"}