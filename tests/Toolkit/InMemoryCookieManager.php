<?php declare(strict_types = 1);

namespace Tests\Toolkit;

use App\Model\Server\Cookie;
use App\Model\Server\ICookieManager;
use App\Model\Utils\Map;
use JetBrains\PhpStorm\Pure;
use function time;

class InMemoryCookieManager implements ICookieManager {

    /** @var Map<string,Cookie> cookies map */
    private Map $cookies;

    #[Pure]
    public function __construct() {
        $this->cookies = new Map();
    }

    #[Pure]
    public function new(string $name): Cookie {
        return new Cookie($this, $name);
    }

    public function get(string $name): mixed {
        $cookie = $this->cookies->get($name);
        if ($cookie instanceof Cookie) {
            if ($cookie->time === 0 || $cookie->time > time()) {
                return $cookie->value;
            }
        }

        return null;
    }

    public function save(Cookie $cookie): bool {
        $this->cookies->put($cookie->name, clone $cookie);

        return $this->cookies->get($cookie->name)?->value === $cookie->value;
    }

}
