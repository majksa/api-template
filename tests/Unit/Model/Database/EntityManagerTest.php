<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Database;

use App\Model\Database\EntityManager;
use App\Model\Entity\Security\User;
use Tester\Assert;
use Tests\Toolkit\BaseContainerTestCase;
use Tests\Toolkit\InMemoryEntityManager;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class EntityManagerTest extends BaseContainerTestCase {

    private EntityManager $em;

    protected function setUp(): void {
        parent::setUp();

        $this->em = $this->getService(InMemoryEntityManager::class);
    }

    public function testRepositoryGetEm(): void {
        Assert::equal($this->em, $this->em->getUserRepository()->getEm());
        $em = new EntityManager($this->em);
        Assert::equal($em->getUserRepository(), $em->getRepository(User::class));
    }

}

(new EntityManagerTest())->run();
