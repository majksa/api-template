<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Utils;

use App\Model\Utils\Arrays;
use Nette\Utils\Random;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;
use function array_key_first;
use function array_values;
use function count;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class ArraysTest extends BaseTestCase {

    /**
     * @param array<mixed,mixed>  $array
     *
     * @dataProvider getTestData
     */
    public function testKeyExists(array $array, int|string $key): void {
        if (Arrays::keyExists($array, $key)) {
            Assert::hasKey($key, $array);
        } else {
            Assert::hasNotKey($key, $array);
        }
    }

    /**
     * @param array<T> $array
     * @param T        $value
     *
     * @template T
     * @dataProvider getTestData
     */
    public function testContains(array $array, mixed $value): void {
        if (Arrays::contains($array, $value)) {
            Assert::contains($value, $array);
        } else {
            Assert::notContains($value, $array);
        }
    }

    /**
     * @param array<T> $array
     * @param T        $value
     *
     * @template T
     * @dataProvider getTestData
     */
    public function testSearch(array $array, mixed $value): void {
        $key = Arrays::search($array, $value);
        if ($key === false) {
            Assert::notContains($value, $array);

            return;
        }

        Assert::hasKey($key, $array);
        Assert::equal($value, $array[$key]);
    }

    /**
     * @param array<T> $array
     * @param T        $value
     *
     * @template T
     * @dataProvider getTestData
     */
    public function testRemove(array $array, mixed $value): void {
        Arrays::remove($array, $value);
        Assert::hasNotKey($value, $array);
    }

    /**
     * @return array<array<mixed>>
     */
    public function getTestData(): array {
        $map = $this->randomArray(5, true);
        $array = $this->randomArray(5, false);

        return [
            [$map, array_values($map)[0]],
            [$map, array_key_first($map)],
            [$map, Random::generate()],
            [$array, $array[0]],
            [$array, array_key_first($array)],
            [$array, 6],
        ];
    }

    /**
     * @return array<string>
     */
    private function randomArray(int $length, bool $associative): array {
        $array = [];
        while (count($array) < $length) {
            if ($associative) {
                $array[Random::generate()] = Random::generate();
            } else {
                $array[] = Random::generate();
            }
        }

        return $array;
    }

}

(new ArraysTest())->run();
