<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Utils;

use App\Model\Utils\Map;
use JetBrains\PhpStorm\Pure;
use stdClass;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class MapTest extends BaseTestCase {

    /**
     * @dataProvider getKeyAndValue
     */
    public function testAll(mixed $key, mixed $value): void {
        $map = new Map();
        $old = $map->put($key, $value);
        Assert::null($old);
        Assert::null($map->get('non existent key'));
        Assert::equal($value, $map->get($key));
        $old = $map->remove($key);
        Assert::null($map->get($key));
        Assert::equal($value, $old);
    }

    /**
     * @return array<array<mixed>>
     */
    #[Pure]
    public function getKeyAndValue(): array {
        $key = new stdClass();
        $key->a = 'b';

        return [
            ['key', 'value'],
            [0, 'value'],
            [true, 'value'],
            [$key, 'value'],
        ];
    }

}

(new MapTest())->run();
