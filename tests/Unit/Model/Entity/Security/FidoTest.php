<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Entity\Security;

use App\Model\Entity\Security\Fido;
use App\Model\Entity\Security\User;
use CodeLts\U2F\U2FServer\Registration;
use JetBrains\PhpStorm\Pure;
use Mockery;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../../Bootstrap.php';

/** @testCase */
final class FidoTest extends BaseTestCase {

    /**
     * @dataProvider getUser
     */
    public function testFido(User $user): void {
        $fido = new Fido('handle', 'public', 'cert', 1);
        $fido2 = new Fido('handle2', 'public2', 'cert2', 1);
        Assert::true($user->fidos->isEmpty());
        $mock = Mockery::mock(Registration::class)->makePartial();
        $mock->setKeyHandle('handle');
        $mock->setPublicKey('public');
        $mock->setCertificate('cert');
        $mock->expects('getCounter')->andReturn(1);
        $user->addFido($mock);
        Assert::equal($fido->handle, $user->fidos->get(0)->handle);
        Assert::equal($fido->public, $user->fidos->get(0)->public);
        Assert::equal($fido->certificate, $user->fidos->get(0)->certificate);
        Assert::notContains($fido2, $user->fidos->toArray());
        Assert::equal($fido->handle, $user->getFidoHandles()[0]->keyHandle);
        Assert::notEqual($fido2->handle, $user->getFidoHandles()[0]->keyHandle);
    }

    /**
     * @return array<array<User>>
     */
    #[Pure]
    public static function getUser(): array {
        return [
            [new User('name', 'surname', 'email', 'phone', 'username', 'password')],
        ];
    }

}

(new FidoTest())->run();
