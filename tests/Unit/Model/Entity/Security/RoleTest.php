<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Entity\Security;

use App\Model\Entity\Security\Permission;
use App\Model\Entity\Security\Role;
use App\Model\Entity\Security\User;
use JetBrains\PhpStorm\Pure;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../../Bootstrap.php';

/** @testCase */
final class RoleTest extends BaseTestCase {

    /**
     * @dataProvider getUser
     */
    public function testAddingRoles(User $user): void {
        $role = new Role('role');
        $role2 = new Role('role2');
        Assert::true($user->roles->isEmpty());
        $user->addRole($role);
        Assert::contains($role, $user->roles->toArray());
        Assert::notContains($role2, $user->roles->toArray());
    }

    /**
     * @dataProvider getUser
     */
    public function testPermissions(User $user): void {
        $permission = new Permission('permission', 'desc');
        $permission2 = new Permission('permission2', 'desc');
        $admin = new Role('admin');
        $admin->allow($permission);
        $admin->allow($permission2);
        $role = new Role('role');
        $role->allow($permission);
        $role2 = new Role('role2');
        $role2->allow($permission2);

        $user->addRole($admin);
        Assert::true($user->isAllowed($permission));
        Assert::true($user->isAllowed($permission2));
        $user->removeRole($admin);

        $user->addRole($role);
        Assert::true($user->isAllowed($permission));
        Assert::false($user->isAllowed($permission2));
        $user->deny($permission);
        Assert::false($user->isAllowed($permission));
        Assert::false($user->isAllowed($permission2));
        $user->addRole($role2);
        Assert::false($user->isAllowed($permission));
        Assert::true($user->isAllowed($permission2));
    }

    /**
     * @return array<array<User>>
     */
    #[Pure]
    public static function getUser(): array {
        return [
            [new User('name', 'surname', 'email', 'phone', 'username', 'password')],
        ];
    }

}

(new RoleTest())->run();
