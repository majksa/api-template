<?php declare(strict_types = 1);

namespace App\Model\Database;

use App\Model\Repository\TRepositories;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Nettrine\ORM\EntityManagerDecorator;

class EntityManager extends EntityManagerDecorator {

    use TRepositories;

    /**
     * @param class-string<T> $className
     * @return EntityRepository<T>
     *
     * @template T of object
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
     * @internal
     */
    public function getRepository($className): ObjectRepository {
        return parent::getRepository($className);
    }

}
