<?php declare(strict_types = 1);

namespace App\Model\Database;

use App\Model\Database\Traits\TId;
use App\Model\Database\Traits\TOnCreate;
use App\Model\Database\Traits\TOnUpdate;

abstract class Entity {

    use TId;
    use TOnCreate;
    use TOnUpdate;

}
