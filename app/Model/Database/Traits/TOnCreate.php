<?php declare(strict_types = 1);

namespace App\Model\Database\Traits;

use App\Model\Utils\Server;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;

trait TOnCreate {

    #[Field]
    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: false, options: ['default' => 'CURRENT_TIMESTAMP'])]
    public DateTimeImmutable $createdAt;

    #[Field]
    #[ORM\Column(type: Types::STRING, nullable: true)]
    public ?string $createdBy;

    #[ORM\PrePersist]
    final public function setCreatedAt(): void {
        $this->createdAt = new DateTimeImmutable();
        $this->createdBy = Server::getClientIp();
    }

}
