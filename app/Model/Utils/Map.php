<?php declare(strict_types = 1);

namespace App\Model\Utils;

use function is_int;

/**
 * @template K
 * @template V
 */
class Map {

    /** @var array<K> $keys */
    private array $keys = [];

    /** @var array<V> $values */
    private array $values = [];

    private int $newIndex = 0;

    /**
     * @param K $key
     * @param V $value
     */
    public function put(mixed $key, mixed $value): mixed {
        $id = $this->findId($key);
        $old = @$this->values[$id] ?: null;
        $this->values[$id] = $value;

        return $old;
    }

    /**
     * @param K $key
     */
    public function remove(mixed $key): mixed {
        $id = $this->findId($key);
        $old = @$this->values[$id] ?: null;
        unset($this->values[$id]);

        return $old;
    }

    /**
     * @param K $key
     * @return V|null
     */
    public function get(mixed $key): mixed {
        return $this->getOrDefault($key, null);
    }

    /**
     * @param K $key
     * @param V|null $value
     * @return V|null found or provided value
     */
    public function getOrDefault(mixed $key, mixed $value): mixed {
        $id = $this->findId($key);
        if (!Arrays::keyExists($this->values, $id)) {
            return $value;
        }

        return $this->values[$id];
    }

    private function findId(mixed $key): int {
        if (is_int($id = Arrays::search($this->keys, $key))) {
            return $id;
        }

        $this->keys[$this->newIndex] = $key;

        return $this->newIndex++;
    }

}
