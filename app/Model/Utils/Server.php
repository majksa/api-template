<?php declare(strict_types = 1);

namespace App\Model\Utils;

use function filter_input;
use const INPUT_SERVER;

final class Server {

    public static function getClientIp(): ?string {
        return filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') ??
            filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') ??
            filter_input(INPUT_SERVER, 'REMOTE_ADDR') ?? null;
    }

}
