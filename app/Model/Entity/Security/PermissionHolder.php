<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @template B of PermissionBinding
 */
#[Type]
abstract class PermissionHolder extends Entity {

    /**
     * @return Collection<int,B>
     */
    #[Pure]
    abstract public function getBindings(): Collection;

    /**
     * @return B
     */
    abstract public function create(Permission $permission): PermissionBinding;

    /**
     * @return B|null
     */
    #[Pure]
    public function getBinding(Permission $permission): ?PermissionBinding {
        foreach ($this->getBindings() as $binding) {
            if ($binding->getHolder() === $this && $binding->permission === $permission) {
                return $binding;
            }
        }

        return null;
    }

    /**
     * @return B
     */
    public function getBindingOrCreate(Permission $permission): PermissionBinding {
        return $this->getBinding($permission) ?: $this->create($permission);
    }

    public function allow(Permission $permission): void {
        $this->updateBinding($permission, PermissionBinding::ALLOW);
    }

    public function deny(Permission $permission): void {
        $this->updateBinding($permission, PermissionBinding::DENY);
    }

    public function reset(Permission $permission): void {
        $this->updateBinding($permission, PermissionBinding::DEFAULT);
    }

    #[Pure]
    public function getValue(Permission $permission): int {
        $binding = $this->getBinding($permission);
        if ($binding instanceof PermissionBinding) {
            return $binding->value;
        }

        return PermissionBinding::DEFAULT;
    }

    #[Pure]
    public function isAllowed(Permission $permission): bool {
        return $this->getValue($permission) === PermissionBinding::ALLOW;
    }

    public function updateBinding(Permission $permission, int $value): void {
        $this->getBindingOrCreate($permission)->value = $value;
    }

}
