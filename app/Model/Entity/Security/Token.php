<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use App\Model\Repository\Security\Token as TokenRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table('token')]
#[ORM\Entity(TokenRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Token extends Entity {

    public function __construct(
        #[ORM\ManyToOne(targetEntity: User::class)]
        #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
        public User $user,
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $token,
    ) {
    }

}
