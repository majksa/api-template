<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use App\Model\Repository\Security\Permission as PermissionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table('permission')]
#[ORM\Entity(repositoryClass: PermissionRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Permission extends Entity {

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 63, unique: true)]
        public string $name,
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
        public string $description,
    ) {
    }

}
