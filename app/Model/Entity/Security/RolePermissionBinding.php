<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @extends PermissionBinding<Role>
 */
#[Type]
#[ORM\Table]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class RolePermissionBinding extends PermissionBinding {

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\ManyToOne(targetEntity: Role::class, inversedBy: 'bindings')]
        #[ORM\JoinColumn(name: 'role_id', referencedColumnName: 'id')]
        public Role $role,
        Permission $permission,
        int $value = self::DEFAULT,
    ) {
        parent::__construct($permission, $value);
    }

    #[Pure]
    public function getHolder(): Role {
        return $this->role;
    }

}
