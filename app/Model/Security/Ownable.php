<?php declare(strict_types = 1);

namespace App\Model\Security;

use App\Model\Entity\Security\User;

interface Ownable {

    public function getOwner(): User;

}
