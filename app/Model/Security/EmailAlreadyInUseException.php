<?php declare(strict_types = 1);

namespace App\Model\Security;

use Exception;
use JetBrains\PhpStorm\Pure;

final class EmailAlreadyInUseException extends Exception {

    #[Pure]
    public function __construct() {
        parent::__construct('Email already in use!', 409);
    }

}
