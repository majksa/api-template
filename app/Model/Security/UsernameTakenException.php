<?php declare(strict_types = 1);

namespace App\Model\Security;

use Exception;
use JetBrains\PhpStorm\Pure;

final class UsernameTakenException extends Exception {

    #[Pure]
    public function __construct() {
        parent::__construct('Username is already taken!', 409);
    }

}
