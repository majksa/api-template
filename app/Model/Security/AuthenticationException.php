<?php declare(strict_types = 1);

namespace App\Model\Security;

use Exception;

final class AuthenticationException extends Exception {

}
