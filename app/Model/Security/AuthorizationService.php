<?php declare(strict_types = 1);

namespace App\Model\Security;

use App\Model\Entity\Security\User;
use Maxa\Ondrej\Nette\GraphQL\DI\Authorization;
use TheCodingMachine\GraphQLite\Security\AuthorizationServiceInterface;

#[Authorization]
final class AuthorizationService implements AuthorizationServiceInterface {

    public function __construct(private Security $security) {
    }

    public function isAllowed(string $right, mixed $subject = null): bool {
        $user = $this->security->getLogged();

        return $user instanceof User && $this->security->isUserAllowed(
            $user,
            $right,
        ) && (!$subject instanceof Ownable || $this->security->isUserAllowed(
            $subject->getOwner(),
            $right,
        ));
    }

}
