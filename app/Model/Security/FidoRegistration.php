<?php declare(strict_types = 1);

namespace App\Model\Security;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type(name: 'FidoRegistrationResponse')]
final class FidoRegistration {

    public function __construct(
        #[Field]
        public string $request,
        #[Field]
        public string $signatures,
    ) {
    }

}
