<?php declare(strict_types = 1);

namespace App\Model\Repository\Security;

use App\Model\Database\Repository;
use App\Model\Entity\Security\User as UserEntity;
use App\Model\Security\EmailAlreadyInUseException;
use App\Model\Security\UsernameTakenException;

/**
 * @extends Repository<UserEntity>
 * @method UserEntity|null findOneByUsername(string $username)
 */
final class User extends Repository {

    /**
     * @throws EmailAlreadyInUseException|UsernameTakenException
     */
    public function checkIfUsernameOrEmailTaken(string $username, string $email): void {
        if ($this->count(['email' => $email]) > 0) {
            throw new EmailAlreadyInUseException();
        }

        if ($this->count(['username' => $username]) > 0) {
            throw new UsernameTakenException();
        }
    }

}
