<?php declare(strict_types = 1);

namespace App\Model\Repository\Security;

use App\Model\Database\Repository;
use App\Model\Entity\Security\Permission as PermissionEntity;

/**
 * @extends Repository<PermissionEntity>
 * @method PermissionEntity|null findOneByName(string $name)
 */
final class Permission extends Repository {

}
