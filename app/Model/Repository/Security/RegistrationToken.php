<?php declare(strict_types = 1);

namespace App\Model\Repository\Security;

use App\Model\Database\Repository;
use App\Model\Entity\Security\RegistrationToken as RegistrationTokenEntity;

/**
 * @extends Repository<RegistrationTokenEntity>
 * @method RegistrationTokenEntity|null findOneByToken(string $token)
 */
final class RegistrationToken extends Repository {

    public function isTokenTaken(string $token): bool {
        return $this->count(['token' => $token]) > 0;
    }

}
