<?php declare(strict_types = 1);

namespace App\Model\Server;

use JetBrains\PhpStorm\Pure;
use function filter_input;
use function setcookie;
use const INPUT_COOKIE;

class CookieManager implements ICookieManager {

    /**
     * Create a new cookie object.
     */
    #[Pure]
    public function new(string $name): Cookie {
        return new Cookie($this, $name);
    }

    /**
     * Return a cookie
     */
    public function get(string $name): mixed {
        return filter_input(INPUT_COOKIE, $name) ?? null;
    }

    /**
     * Create or Update cookie.
     */
    public function save(Cookie $cookie): bool {
        return setcookie(
            $cookie->name,
            $cookie->value,
            $cookie->time,
            $cookie->path,
            $cookie->domain,
            $cookie->secure,
            true,
        );
    }

}
