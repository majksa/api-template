<?php declare(strict_types = 1);

namespace App\Model\Server;

interface ICookieManager {

    public function new(string $name): Cookie;

    public function get(string $name): mixed;

    public function save(Cookie $cookie): bool;

}
